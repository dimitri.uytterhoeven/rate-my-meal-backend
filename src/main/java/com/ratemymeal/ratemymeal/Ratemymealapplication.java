package com.ratemymeal.ratemymeal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ratemymealapplication {

	public static void main(String[] args) {
		SpringApplication.run(Ratemymealapplication.class, args);
	}

}
