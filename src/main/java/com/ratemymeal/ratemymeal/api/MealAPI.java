package com.ratemymeal.ratemymeal.api;

import com.ratemymeal.ratemymeal.entity.Meal;
import com.ratemymeal.ratemymeal.service.MealService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin("http://vps743766.ovh.net")
@RequestMapping("/api/v1/meals")
@Slf4j
@RequiredArgsConstructor
public class MealAPI {

    @Autowired
    private MealService mealService;

    @GetMapping
    public ResponseEntity<ApiDTO<List<Meal>>> findAll() {
        ApiDTO<List<Meal>> response = new ApiDTO<>(mealService.findAll());
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Meal meal) {
        return ResponseEntity.ok(mealService.save(meal));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiDTO<Meal>> findById(@PathVariable Long id) {
        Optional<Meal> meal = mealService.findById(id);
        if (!meal.isPresent()) {
//            log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }

        ApiDTO<Meal> response = new ApiDTO<>(meal.get());

        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Meal> update(@PathVariable Long id, @Valid @RequestBody Meal meal) {
        if (!mealService.findById(id).isPresent()) {
//            log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(mealService.save(meal));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!mealService.findById(id).isPresent()) {
//            log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }

        mealService.deleteById(id);

        return ResponseEntity.ok().build();
    }
}
