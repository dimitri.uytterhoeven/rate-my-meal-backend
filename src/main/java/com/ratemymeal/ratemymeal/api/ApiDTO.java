package com.ratemymeal.ratemymeal.api;

import lombok.Data;

@Data
public class ApiDTO<T> {
    private T goal;

    public ApiDTO(T goal) {
        this.goal = goal;
    }

}
