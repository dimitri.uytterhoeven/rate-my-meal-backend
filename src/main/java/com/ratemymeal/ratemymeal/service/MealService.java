package com.ratemymeal.ratemymeal.service;

import com.ratemymeal.ratemymeal.entity.Meal;
import com.ratemymeal.ratemymeal.repository.MealRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MealService {

    @Autowired
    private MealRepository mealRepository;

    public List<Meal> findAll() {
        return mealRepository.findAll();
    }

    public Optional<Meal> findById(Long id) {
        return mealRepository.findById(id);
    }

    public Meal save(Meal meal) {
        return mealRepository.save(meal);
    }

    public void deleteById(Long id) {
        mealRepository.deleteById(id);
    }
}
