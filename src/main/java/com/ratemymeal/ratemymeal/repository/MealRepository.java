package com.ratemymeal.ratemymeal.repository;

import com.ratemymeal.ratemymeal.entity.Meal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MealRepository extends JpaRepository<Meal, Long> {
}
