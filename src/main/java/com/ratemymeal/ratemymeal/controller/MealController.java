package com.ratemymeal.ratemymeal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin("http://vps743766.ovh.net")
public class MealController {
    @GetMapping("/")
    public String list(){
        return "meals";
    }
}