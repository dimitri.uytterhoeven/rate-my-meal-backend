CREATE TABLE `meal` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
   `content` longtext,
   `created_at` datetime(6) DEFAULT NULL,
   `image` longtext,
   `rate` decimal(19,2) DEFAULT NULL,
   `title` varchar(255) DEFAULT NULL,
   `updated_at` datetime(6) DEFAULT NULL,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
